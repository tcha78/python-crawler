from urllib.parse import urlparse

def get_domain(url):
    try:
        results = get_sub_domain(url).split('.')
        return results #get only sites with same subdomain
    except:
        print('Couldnt get domain for ' + url)

def get_sub_domain(url):
    try:
        return urlparse(url).netloc
    except:
        print('Couldnt get subdomain for ' + url)
