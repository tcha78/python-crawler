import threading
from queue import Queue
from crawler import Crawler
from domain import *
from general import *

PROJECT_NAME = 'Playstation'
HOMEPAGE = 'https://www.playstation.com/en-us/games/'
DOMAIN_NAME = get_domain(HOMEPAGE)
QUEUE_FILE = PROJECT_NAME + '/queue.txt'
CRAWLED_FILE = PROJECT_NAME + '/crawled.txt'
NUMBER_OF_THREADS = 2
queue = Queue()
Crawler(PROJECT_NAME, HOMEPAGE, DOMAIN_NAME)

# Create worker threads
def create_workers():
    for _ in range(NUMBER_OF_THREADS):
        t = threading.Thread(target=work)
        t.daemon = True
        t.start()

# Next job in queue
def work():
    while True:
        url = queue.get()
        Crawler.crawl_page(threading.current_thread().name, url)
        queue.task_done()

# Each queued link is a new job
def create_jobs():
    for link in file_to_set(QUEUE_FILE): 
        queue.put(link)
    queue.join()
    #go deeper
    #crawl()

# Check queue, if items in qeueue, crawl
def crawl():
    queued_links = file_to_set(QUEUE_FILE)
    if len(queued_links) > 0:
        print(str(len(queued_links)) + ' links in the queue')
        create_jobs()

create_workers()
crawl()
